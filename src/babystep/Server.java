package babystep;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private int port;
	private ServerSocket srv;
	
	public Server(int port) throws IOException {
		this.port = port;
		this.srv = new ServerSocket(this.port);
	}
	
	public void loop() throws IOException {		
		while (true) {
			Socket clt = this.srv.accept();
			
			OutputStream		os	= clt.getOutputStream();
			DataOutputStream	dos	= new DataOutputStream(os);
			
			InputStream		is	= clt.getInputStream();
			DataInputStream	dis	= new DataInputStream(is);
			
			String msg = "Hello " + dis.readUTF();
			
			dos.writeUTF(msg);
			dos.flush();
			
			dos.close();
			dis.close();
			clt.close();
			
			System.out.println("Done");
			
		}
	}

	public static void main(String[] args) throws IOException {
		Server srv = new Server(1234);
		srv.loop();
	}

}
