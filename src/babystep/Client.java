package babystep;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.net.Socket;


public class Client {

	private String	srvAddr;
	private int		srvPort;
	private Socket	srv;

	public Client(String addr, int port) throws IOException {
		this.srvAddr	= addr;
		this.srvPort	= port;
		this.srv		= new Socket(this.srvAddr, this.srvPort);
	}

	public void run(String name) throws IOException {
		OutputStream		os	= this.srv.getOutputStream();
		InputStream			is	= this.srv.getInputStream();
		DataOutputStream	dos	= new DataOutputStream(os);
		DataInputStream		dis	= new DataInputStream(is);

		dos.writeUTF(name);
		dos.flush();

		System.out.println("SERVER>> " + dis.readUTF());

		dos.close();
		dis.close();
		this.srv.close();
	}

	public static void main(String[] args) throws IOException {
		Client client = new Client("localhost", 1234);
		client.run("Toto");
	}
}
