package serverfile.multithread.poolbased.consprdbuff;

import java.net.Socket;

public class ServerBuffer {
	
	private int	bufferSize;
	private int	in, out;
	private int	nbCurent;

	private Socket buff[];

	/**
	 * ServerBuffer Constructor
	 * @param sz Size of buffer
	 */
	public ServerBuffer(int sz) {
		this.bufferSize	= sz;
		this.nbCurent	= 0;
		this.in			= 0;
		this.out		= 0;
		this.buff		= new Socket[this.bufferSize];
	}
	
	synchronized public void put(Socket s) throws InterruptedException {
		while(this.nbCurent >= this.bufferSize)
			wait();
		
		this.buff[this.in] = s;
		this.in = (this.in + 1) % this.bufferSize;
		this.nbCurent++;
		
		notifyAll();
	}
	
	synchronized public Socket get() throws InterruptedException {
		while(this.nbCurent < 1)
			wait();
		
		Socket s = this.buff[this.out];
		this.out = (this.out + 1) % this.bufferSize;
		this.nbCurent--;
		
		notifyAll();
		
		return s;
	}
}
