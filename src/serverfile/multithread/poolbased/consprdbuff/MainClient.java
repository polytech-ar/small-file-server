package serverfile.multithread.poolbased.consprdbuff;

import java.io.IOException;
import java.net.UnknownHostException;

public class MainClient {
	public static void main(String[] args) throws UnknownHostException, IOException {
		Client clt = new Client(args[0], Integer.valueOf(args[1]), args[2]);
		clt.run();
	}
}
