package serverfile.multithread.poolbased.blockingqueue;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import serverfile.basic.Instruction;

public class Client extends Thread{

	// Chunk size
	private static final int CHUNK_SIZE = 512;

	private String	fileName;	/* Name of file to download */
	private String	newName;	/* Local name of file */
	private Socket	srv;		/* Socket to server */

	/**
	 * Constructor of Client
	 * 
	 * @param srvAddr  Server address
	 * @param srvPort  Server port
	 * @param fileName The name of the fileName to download
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Client(String srvAddr, int srvPort, String fileName) throws UnknownHostException, IOException {
		this(srvAddr, srvPort, fileName, fileName);
	}

	/**
	 * Constructor of Client
	 * 
	 * @param srvAddr  Server address
	 * @param srvPort  Server port
	 * @param fileName The name of the fileName to download
	 * @param newName  The name of the local fileName
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Client(String srvAddr, int srvPort, String fileName, String newName)
			throws UnknownHostException, IOException {
		this.srv		= new Socket(srvAddr, srvPort);
		this.fileName	= fileName;
		this.newName	= newName;
	}

	public void run() {
		try {
			OutputStream		os	= this.srv.getOutputStream();
			InputStream			is	= this.srv.getInputStream();
			DataOutputStream	dos	= new DataOutputStream(os);
			DataInputStream		dis	= new DataInputStream(is);

			// We send the name of wanted file to the server
			dos.writeUTF(this.fileName);
			dos.flush();

			// We wait and read the server's answer
			String msg = dis.readUTF();

			// If the server's answer is only an instruction and she is the File not found
			// error then we print the error.
			if (msg.equals(Instruction.FILE_NOT_FOUND.toString())) {
				System.out.println("SERVER>> Error: File not found");
			} else {
				int		tmpFileLength	= 0;		// Size of the tmp file
				int		nread			= 0;		// NB bytes read
				boolean	stop			= false;	// Boolean to stop the while loop in case of error

				// We check if a tmp file is already exist
				File f = new File("tmp." + this.newName);
				if (f.exists()) { // If yes we get his length
					tmpFileLength = (int) f.length();
				} else { // If no we create it
					f.createNewFile();
				}

				FileOutputStream		fos	= new FileOutputStream(f, true);
				BufferedOutputStream	bos	= new BufferedOutputStream(fos);

				// We read the file's length sent by the server
				int length = dis.readInt();

				// We send the length of the tmp file as offset
				dos.writeInt(tmpFileLength);
				dos.flush();

				// We read all chunk sent by the server
				while (nread < length && !stop) {
					byte	buff[]	= new byte[CHUNK_SIZE];
					int		n		= 0;

					try {
						n = dis.read(buff, 0, CHUNK_SIZE);

						if (n == -1) { // Case end of file
							nread = length;
							bos.write(buff);
						} else {
							nread += n;
							bos.write(buff, 0, n);
//						stop = true; // Uncomment this line to simulate a failure
						}
					} catch (IOException ex) {
						System.err.println("Connection lost! Pleas retry later.");
						stop = true;
					}

				}

				// We close every output stream linked to the file
				bos.close();
				fos.close();

				if (nread >= length && !stop) { // Case where the local file is complete
					// We rename the tmp file
					f.renameTo(new File(this.newName));
					// Message to the user to tell the task is completed
					System.out.println("CLIENT>> Dowload completed!");
				} else { // Case a failure happens
					System.out.println("Part of the file is saved until the next attempt is successful.");
				}
			}

			// We close the connection
			dos.close();
			dis.close();
			os.close();
			is.close();
			this.srv.close();
		} catch (IOException e) {

		}
	}
}
