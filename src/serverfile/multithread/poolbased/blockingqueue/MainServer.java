package serverfile.multithread.poolbased.blockingqueue;

import java.io.IOException;

public class MainServer {
	public static void main(String[] args) throws IOException, InterruptedException {
		Server srv = new Server(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2]),args[3]);
		srv.run();
	}
}
