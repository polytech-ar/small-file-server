package serverfile.multithread.poolbased.blockingqueue;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;

public class Server {

	private int	port;
	private int	poolSize;

	private String						path;
	private ServerSocket				srv;
	private ArrayBlockingQueue<Socket>	srvBuff;
	private Thread						pool[];

	/**
	 * Constructor of Server
	 * 
	 * @param poolSize Size of the tread pool
	 * @param sizeBuff Size of the server buffer
	 * @param port     Listening port of the server
	 * @param path     Path where to look for the file requested by the client
	 * @throws IOException
	 */
	public Server(int poolSize, int sizeBuff, int port, String path) throws IOException {

		if (poolSize < 1) {
			throw new IllegalArgumentException("The size value sould be higher than 0");
		}

		this.poolSize	= poolSize;
		this.port		= port;
		this.path		= path.isEmpty() ? "/" : path;
		this.srvBuff	= new ArrayBlockingQueue<Socket>(sizeBuff);
		this.srv		= new ServerSocket(this.port);
		this.pool		= new ServerWorker[this.poolSize];

	}

	public void run() throws IOException, InterruptedException {
		for (int i = 0; i < this.poolSize; i++) {
			this.pool[i] = new ServerWorker(this.path, this.srvBuff);
			this.pool[i].setDaemon(true);
			this.pool[i].start();
		}
		
		while (true) {
			this.srvBuff.put(this.srv.accept());
		}
	}
}
