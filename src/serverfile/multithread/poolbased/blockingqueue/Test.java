package serverfile.multithread.poolbased.blockingqueue;

import java.io.IOException;
import java.net.UnknownHostException;

public class Test {
	public static void main(String[] args) throws UnknownHostException, IOException {
		int N = 60; // Last value before troubles
		Client clts[] = new Client[N];
		
		String filename = "poste.pdf";
		String newfilname;
		for(int i = 0; i < N; i++) {
			newfilname = i+".pdf";
			clts[i] = new Client("localhost",1234, filename, newfilname);
			clts[i].setDaemon(true);
		}
		
		for(int i = 0; i < N; i++) {
			System.out.println("Launch clt n° "+i);
			clts[i].start();
		}
	}
}
