package serverfile.multithread.basic;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {

	private int				port;
	private ServerSocket	srv;
	private String			path;

	/**
	 * Constructor of Server
	 * 
	 * @param port Listening port of the server
	 * @param path Path where to look for the file requested by the client
	 * @throws IOException
	 */
	public Server(int port, String path) throws IOException {
		this.port	= port;
		this.srv	= new ServerSocket(this.port);
		this.path	= path.isEmpty() ? "/" : path;
	}

	public void run() throws IOException {
		while (true) {
			// We create a thread for each client.
			Thread t = new ServerWorker(this.srv.accept(), this.path);
			t.setDaemon(true);
			t.start();
		}
	}
}
