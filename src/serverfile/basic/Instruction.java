package serverfile.basic;

/*
 * I created this enumeration to make the protocol scalable. 
 * To make it easy to manage new features
 */
public enum Instruction {
	OK("OK"), // Instruction to say keep going (Like acknowledgment of TCP)
	FILE_NOT_FOUND("FNF"); // Instruction to say find not found

	private String name;

	Instruction(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
