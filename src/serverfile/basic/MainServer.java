package serverfile.basic;

import java.io.IOException;

public class MainServer {
	public static void main(String[] args) throws IOException {
		Server srv = new Server(Integer.valueOf(args[0]),args[1]);
		srv.run();
	}
}
