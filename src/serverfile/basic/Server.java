package serverfile.basic;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server {

	// Chunk size
	private static final int CHUNK_SIZE = 512;

	private ServerSocket	srv;	/* Server socket */
	private String			path;	/* Base path where look for the requested file */

	/**
	 * Constructor of Server
	 * 
	 * @param port Listening port of the server
	 * @param path Path where to look for the file requested by the client
	 * @throws IOException
	 */
	public Server(int port, String path) throws IOException {
		this.srv = new ServerSocket(port);

		// We check if the given path is not empty.
		this.path = path.isEmpty() ? "/" : path;
	}

	public void run() throws IOException {
		while (true) {
			Socket				clt	= this.srv.accept();
			OutputStream		os	= clt.getOutputStream();
			InputStream			is	= clt.getInputStream();
			DataOutputStream	dos	= new DataOutputStream(os);
			DataInputStream		dis	= new DataInputStream(is);

			String msg = dis.readUTF();

			File f = new File(this.path + msg);

			// We check a file with that name exists and if he is not a directory.
			if (f.exists() && !f.isDirectory()) {

				dos.writeUTF(Instruction.OK.toString());

				FileInputStream		fis	= new FileInputStream(f);
				BufferedInputStream	bif	= new BufferedInputStream(fis);

				// We get the length file and we send it
				int fileLength = (int) f.length();
				// We send the length of file to the client.
				dos.writeInt(fileLength);
				dos.flush();

				// We read the offset sent by the client
				int rwrite = dis.readInt();
				// We change the pointer's position to the offset
				bif.read(new byte[rwrite], 0, rwrite);

				try {
					// We read the file and send each chunk to the client
					while (rwrite < fileLength) {
						byte	content[]	= new byte[CHUNK_SIZE];
						int		n			= bif.read(content, 0, CHUNK_SIZE);

						if (n == -1) { // Case end of file
							rwrite = fileLength;
							dos.write(content);
						} else {
							rwrite += n;
							dos.write(content, 0, n);
						}
					}
				} catch (SocketException ex) {
					System.out.println("Connection lost!");
				}

				// We close all InputStream linked to the file
				bif.close();
				fis.close();

			} else {

				// If the file not exists or if there is a directory with that name then we send
				// to the client the file not found error.
				dos.writeUTF(Instruction.FILE_NOT_FOUND.toString());
				dos.flush();

			}

			// We close the connection
			dos.close();
			dis.close();
			is.close();
			os.close();
			clt.close();
		}
	}
}
